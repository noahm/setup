#!/bin/bash

ARCH=$1

# Pull in the core settings for all builds
. settings.sh
. weekly.sh
. common.sh


# Set up the directories etc. needed for a named arch in the weekly
# build
if [ "$DRYRUN"x != ""x ]; then
    echo "Dry-run $0: ARCH $ARCH"
    exit 0
fi

rm -rf ${PUBDIRJIG}/$ARCH ${PUBDIRJIG}-firmware/$ARCH
for i in iso-cd jigdo-cd; do
    mkdir -p ${PUBDIRJIG}/$ARCH/$i
    if $(arch_has_firmware $ARCH) ; then
	mkdir -p ${PUBDIRJIG}-firmware/$ARCH/$i
    fi
done
