#!/bin/bash

ARCH=$1

# Pull in the core settings for all builds
. settings.sh
. daily.sh
. common.sh

# Set up the directories etc. needed for a named arch in the weekly
# build

OUT_DIR=$OUT_FREE_SID
INFILE=$TOPDIR/daily.html

if [ "$DRYRUN"x != ""x ]; then
    echo "Dry-run $0: ARCH $ARCH"
    exit 0
fi

ARCH_DIR=$OUT_DIR/$ARCH

# Only keep things / do any work here if there are any files in the ARCH_DIR
NUM_FILES=`find $ARCH_DIR -type f | wc -l`
if [ $NUM_FILES = 0 ] ; then
    rm -rf $ARCH_DIR
    exit 0
fi

# else
ARCH_ISO_DIR=${ARCH_DIR}/iso-cd
ARCH_JIGDO_DIR=${ARCH_DIR}/jigdo-cd
DATESTRING=`date -u`

sed "s/ARCH/$ARCH/g;s/DATE/$DATESTRING/g;s/BUILDNUM/$BUILDNUM/g;s/CODENAME/$CODENAME/g" \
    $INFILE > $ARCH_DIR/HEADER.html

generate_checksums_for_arch $ARCH $ARCH_JIGDO_DIR
~/build.${CODENAME}/generate_headers ~/build.${CODENAME}/HEADER.html.in $ARCH_DIR $ARCH
	
cd $ARCH_DIR
echo "$ARCH: Signing checksums files using the automatic key"
~/build.${CODENAME}/sign-images $OUT_DIR $ARCH
~/build.${CODENAME}/mklist iso-cd/*iso
