#!/usr/bin/make -f

export BUILDROOT=$(shell pwd)
export VM_CONFIG_AMD64_LIVE=/home/debian-cd/.live-vm-config
export BUILDS_LOG=/home/debian-cd/.weekly_builds_log

# Explicit do-nothing build if we haven't specified any targets
all:
	@echo "No targets specified, doing nothing"

# All the targets - finalised groups of architecture builds. _F suffix
# is the firmware-included non-free build.
all_inst: F_amd64 F_amd64_F F_i386 F_i386_F \
	F_multi-arch F_multi-arch_F \
	F_arm64 F_armhf F_armel F_source \
	F_ppc64el F_mipsel F_mips64el F_s390x \
	firmware_images_bullseye

all_live: FL_amd64 FL_i386 FL_source FL_amd64_F FL_i386_F FL_source_F
	@echo "stop live VM"
	@~/live/bin/vm-stop -c $(VM_CONFIG_AMD64_LIVE)
	@rm -vf $(VM_CONFIG_AMD64_LIVE)

live_desktops = cinnamon gnome kde lxde lxqt mate standard xfce

TL_amd64::    $(foreach dt,$(live_desktops),BL_amd64_$(dt))
TL_i386::     $(foreach dt,$(live_desktops),BL_i386_$(dt))
TL_source::   $(foreach dt,$(live_desktops),BL_source_$(dt))
TL_amd64_F::  $(foreach dt,$(live_desktops),BL_amd64_$(dt)_F)
TL_i386_F::   $(foreach dt,$(live_desktops),BL_i386_$(dt)_F)
TL_source_F:: $(foreach dt,$(live_desktops),BL_source_$(dt)_F)

# Try to impose some ordering, while still staying parallel. We want
# to get amd64, i386, multi-arch and arm64 out early so we test those
# first while other builds are still going.
EARLY_TARGETS = T_amd64 T_amd64_F T_i386 T_i386_F T_multi-arch T_multi-arch_F T_arm64 F_amd64 F_amd64_F
arch_setup_source::	| $(EARLY_TARGETS)
arch_setup_armhf::	| $(EARLY_TARGETS)
arch_setup_armel::	| $(EARLY_TARGETS)
arch_setup_mipsel::	| $(EARLY_TARGETS)
arch_setup_mips64el::	| $(EARLY_TARGETS)
arch_setup_ppc64el::	| $(EARLY_TARGETS)
arch_setup_s390x::	| $(EARLY_TARGETS)

# Similarly, try to order the live builds a bit. Absolute top priority
# is to have the amd64 live builds out first. Source builds already
# depend on the respective amd64 and i386 builds, so don't need to be
# listed here.
ifneq ($(NOINST),1)
TL_amd64::   | $(EARLY_TARGETS)
endif
TL_i386::    | FL_amd64
TL_amd64_F:: | FL_i386
TL_i386_F::  | FL_amd64_F

# T_<arch>
# List all the Targets for each arch for the main weekly build.
# 
# Ordering of the component names is important - config is built up
# left-to-right. In all cases, list the components in order as:
# ARCH_DISKTYPE_option1_option2_...(firmware)
#
# where (firmware) is optional
#
# There is an option to over-ride that config *last* using a config
# named to match the full build name with, including the leading B_
# and a trailing ".cfg", e.g. "B_i386_netinst.cfg"
T_amd64: B_amd64_dvd B_amd64_bd B_amd64_dlbd \
	 B_amd64_usb16 B_amd64_netinst B_amd64_netinst_mac \
	 B_amd64_netinst_edu B_amd64_usb_edu
T_i386: B_i386_dvd B_i386_bd B_i386_dlbd \
	B_i386_usb16 B_i386_netinst B_i386_netinst_mac \
	B_i386_netinst_edu B_i386_usb_edu
T_multi-arch: B_multi-arch_netinst
T_arm64: B_arm64_dvd B_arm64_netinst
T_armhf: B_armhf_dvd B_armhf_netinst
T_armel: B_armel_dvd B_armel_netinst
T_source: B_source_dvd B_source_bd B_source_dlbd
T_ppc64el: B_ppc64el_dvd B_ppc64el_netinst
T_mipsel: B_mipsel_dvd B_mipsel_netinst
T_mips64el: B_mips64el_dvd B_mips64el_netinst
T_s390x: B_s390x_dvd B_s390x_netinst

# Targets for the firmware weekly build
T_amd64_F: B_amd64_netinst_firmware \
	   B_amd64_dvd_firmware \
	   B_amd64_netinst_edu_firmware \
	   B_amd64_usb_edu_firmware
T_i386_F:  B_i386_netinst_firmware \
	   B_i386_dvd_firmware \
	   B_i386_netinst_edu_firmware \
	   B_i386_usb_edu_firmware
T_multi-arch_F: B_multi-arch_netinst_firmware

# Helpers to give the number of builds
count_live:
	@${MAKE} -f Makefile.weekly all_live DRYRUN=count NUM_BUILDS=0 | grep run-live-build-binary | wc -l 
count_inst:
	@${MAKE} -f Makefile.weekly all_inst DRYRUN=count NUM_BUILDS=0 | \
		grep -e weekly-build-wrapper -e generate_firmware_images | \
		wc -l

# The stuff needed before running any of the per-arch setup
inst_setup:
	@./inst-setup.sh

# Do all the initial setup for a given arch, run *once* before all the
# individual build targets for that arch
arch_setup_%:: inst_setup
	@./weekly-build-setup-arch.sh $(word 3,$(subst _, ,$@))

firmware_images_bullseye:
	@./generate_firmware_images bullseye

# F_<arch>
# The Finalise targets; each depends on all the targets for a given
# arch, then does the final bits needed to pull together all those
# builds into a complete set, sync them, etc.
F_%: T_%
	@./weekly-build-finalise-arch.sh \
		$(word 2,$(subst _, ,$@)) $(word 3,$(subst _, ,$@))

FL_%: TL_%
	@~/live/bin/finalise-live-arch.sh -c $(VM_CONFIG_AMD64_LIVE) -B $@

start_live_vm: firmware_images_bullseye
	@echo "starting VM for live builds, storing conf in $(VM_CONFIG_AMD64_LIVE)"
	@~/live/bin/vm-start -a amd64 -r ${CODENAME} -t live -o $(VM_CONFIG_AMD64_LIVE)

# The calls to $$(subst...) below need to come after .SECONDEXPANSION,
# they depend on a second pass of the GNU make parser
.SECONDEXPANSION:
B_%:  arch_setup_$$(word 2,$$(subst _, ,$$@))
	@./weekly-build-wrapper.sh -l $(BUILDS_LOG) -n $(NUM_BUILDS) -B $@

BL_source_%_F: BL_amd64_$$(word 3,$$(subst _, ,$$@))_F BL_i386_$$(word 3,$$(subst _, ,$$@))_F
	@~/live/bin/run-live-build-binary \
		-c $(VM_CONFIG_AMD64_LIVE) \
		-l $(BUILDS_LOG) \
		-r $(CODENAME) \
		-B $@ \
		-n $(NUM_BUILDS)

BL_source_%: BL_amd64_$$(word 3,$$(subst _, ,$$@)) BL_i386_$$(word 3,$$(subst _, ,$$@))
	@~/live/bin/run-live-build-binary \
		-c $(VM_CONFIG_AMD64_LIVE) \
		-l $(BUILDS_LOG) \
		-r $(CODENAME) \
		-B $@ \
		-n $(NUM_BUILDS)

BL_%:   start_live_vm
	@~/live/bin/run-live-build-binary \
		-c $(VM_CONFIG_AMD64_LIVE) \
		-l $(BUILDS_LOG) \
		-r $(CODENAME) \
		-B $@ \
		-n $(NUM_BUILDS)

clean_builds_log:
	@ rm -f $(BUILDS_LOG) $(BUILDS_LOG).lock ; touch $(BUILDS_LOG)

.PHONY: B_*
.PHONY: T_*
.PHONY: BL_*
.PHONY: TL_*
